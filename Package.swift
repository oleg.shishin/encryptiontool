// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EncryptionTool",
    products: [
        .executable(name: "encryptionTool", targets: ["EncryptionTool"])
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-tools-support-core.git", from: "0.1.12"),
        .package(url: "https://github.com/apple/swift-argument-parser.git", from: "0.3.1"),
//        .package(url: "https://github.com/krzyzanowskim/CryptoSwift.git", from: "0.15.0")
    ],

    targets: [
        .target(
            name: "EncryptionTool",
            dependencies: [
                .product(name: "ArgumentParser", package: "swift-argument-parser"),
                .product(name: "SwiftToolsSupport", package: "swift-tools-support-core"),
//                .product(name: "CryptoSwift", package: "CryptoSwift")

            ]),

        .testTarget(
            name: "EncryptionToolTests",
            dependencies: ["EncryptionTool"]),
    ]
)

