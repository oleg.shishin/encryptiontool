import XCTest

import EncryptionToolTests

var tests = [XCTestCaseEntry]()
tests += EncryptionToolTests.allTests()
XCTMain(tests)
